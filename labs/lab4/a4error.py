import json
import requests

url = 'https://www.googleapis.com/compute/v1/projects/cs495-alexander-dupree/zones/us-west1-b/instances/a4-instance/setMetadata'

access_token = ''

# Temporary ssh key, it has been deleted already
a4_key = ''

fingerprint = ''

headers = {
    'Authorization' : f'Bearer {access_token}',
    'Accept' : 'application/json',
    'Content-TYpe' : 'application/json'
}

data = {
    "fingerprint" : f"{fingerprint}",
    "items" : [{
        "key" : "ssh-keys",
        "value" : f"ubuntu:{a4_key} ubuntu"
    }]
}

resp = requests.post(url, data=json.dumps(data), headers=headers)
print(resp.text)