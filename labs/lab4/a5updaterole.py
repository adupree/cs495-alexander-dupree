import json
import requests

url = 'https://iam.googleapis.com/v1/projects/cs495-alexander-dupree/roles/a5_access_role_588018255234?updateMask=includedPermissions'

access_token = 'ya29.c.KqUBvwc3Q4exEL7SGMO2u260mjSWXnrZDzbxubHl4CU17LCAVhXW-44ThtdZXkFcPSVV1PeFhNlvj25y_j0tUYWxTsebJLhfhe3bSWMxKcV04Gz3TZ34YFTzfhgWehdndojCQaaMiTARlIGgckxxi2litg-bb7P_KwqMDkpXCI1yyg2XOyeJE80XqzOMLZR5-sGagftIEF8amXOLK9OnxrgczcsZ3sNg'

headers = {
    'Authorization' : f'Bearer {access_token}',
    'Accept' : 'application/json',
    'Content-Type' : 'application/json'
}

data = {
    "includedPermissions" : [
        "storage.objects.get",
        "storage.objects.list",
        "storage.buckets.get",
        "storage.buckets.list"
    ]
}

resp = requests.patch(url, data=json.dumps(data), headers=headers)
print(resp.text)