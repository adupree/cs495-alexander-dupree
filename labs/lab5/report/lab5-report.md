# **CS495 - Lab 5 Report**
### *Author: Alexander DuPree*

---

# **5.1 - Tools Setup**

Landing page for lamp-bitnami deployment
![tools-setup](../images/lamp-bitnami.png)

Landing page for nginxbbitnami deployment
![tools-setup](../images/lamp-bitnami.png)

lsof results for lamp stack
![tools-setup](../images/lamp-lsof.png)

lsof results for nginx stack
![tools-setup](../images/nginix-lsof.png)

nginx config
![tools-setup](../images/nginix-config.png)

Lamp document root
![tools-setup](../images/lamp-document-root.png)

# **5.1 - wfuzz, nmap, bucket-stream**

## **wfuzz**

wfuzz scan of lamp stack vm
![wfuzz](../images/wfuzz-lampstack.png)

wfuzz scan of nginix server
![wfuzz](../images/wfuzz-nginxstack.png)

wfuzz scan of wfp1 
![wfuzz](../images/wfuzz-wfp1.png)

wfuzz scan of wfp2
![wfuzz](../images/wfuzz-wfp2.png)

wfuzz scan of the windows r2 server
![wfuzz](../images/wfuzz-windows.png)

## **nmap**

nmap ip scan with a range of addressess
![nmap](../images/nmap-ip-scan.png)

A quick google of Aapache 2.2.22 reveals it was released in 
2014. So wfp1 vm is at least 6 years old. 
![nmap](../images/wfp1-nmap-scan.png)

The `-A` options reveals much more about the server than `-sV`
![nmap](../images/wfp1-nmap-scan-2.png)

nmap script to brute force word press
![wpscan](../images/nmap-wordpress.png)

nmap script that checks authentication methods supported by a server
![nmap](../images/nmap-ssh-auth.png)

nmap script to brute force ssh connection
![nmap](../images/nmap-sssh-brute.png)

nmap, http-sitemap-generator matches wfuzz scan of the same site
![nmap](../images/nmap-http-sitemap.png)

## **bucket-stream**

Scanning buckets with bucket-stream
![nmap](../images/bucket-stream3.png)

Accessing open buckets manifest
![nmap](../images/bucket-stream1.png)

Downloading file from bucket via the key
![nmap](../images/bucket-stream2.png)

# **5.3 - wpscan**

Setting up wordpress 4.6
![wpscan](../images/wordpress1.png)

Marketplace wordpress installation
![wpscan](../images/wordpress2.png)

wordpress 4.6 wpscan
![wpscan](../images/wordpress46-scan.png)

Marketplace wordpress wpscan
![wpscan](../images/wordplace-marketplace-scan.png)

# **5.4 - hyrda, sqlmap, xssstrike, w3af, commix**

## **hydra**

Brute forcing the wfp2 authentication example 1 page
![hydra](../images/hydra.png)

## **sqlmap**

Injection points for wfp1 sql injeciton example 1
![sqlmap](../images/wfp1-sqlmap-injection-point.png)

Dump of the wfp1 users table
![sqlmap](../images/wfp1-sqlmap-user-table.png)

Output of running against the white space filtered exercise with tamper module `space2randomblank`
![sqlmap](../images/sqlmap-2.png)

## **XSS Strike**

XSS Strike high efficiency payload
![xsstrike](../images/xssstrike1.png)

Exploiting wfp1 with suggested payload
![xsstrike](../images/xssstrike2.png)

3 Different URL's and their payloads from the firing range
![xsstrike](../images/xssstrike-url1.png)
![xsstrike](../images/xssstrike-url2.png)
![xsstrike](../images/xssstrike-url3.png)

## **w3af**

Command injection detection on wfp1
![w3af](../images/w3af-os-scan.png)

XSS audit with w3af on wfp1
![w3af](../images/w3af-wfp1-xss.png)

XSS audit with w3af on url from firing range
![w3af](../images/w3af-xss.png)

Local and Remote file include scan on wfp1
![w3af](../images/w3af-lfi-rfi.png)

## **commix**

Commix command injection on wfp1
![commix](../images/commix.png)

# **5.5 - Metasploit** 

## **Metasploit Apache Struts**

Reverse shell to apache struts application
![msf](../images/ms-struts1.png)

Environment Variables for struts instance
![msf](../images/ms-struts-environ.png)

## **Metasploit Directory Scan**

Directory Scan of wfp1
![msf](../images/ms-dir-scanner.png)

## **Metasploit Credential Stuffing**

Brute forcing http-login with Metasploit
![msf](../images/ms-http-login.png)
