\documentclass[11pt,letterpaper]{report}
\usepackage[latin1]{inputenc}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{graphicx}
\usepackage{color}
\usepackage{enumitem}
\usepackage[dvipsnames]{xcolor}
\definecolor{codegray}{gray}{0.9}
\newcommand{\code}[1]{\colorbox{codegray}{\texttt{#1}}}
\graphicspath{{../images/}{IR}}
%\newcommand{\LF}{}  % turn on to display large format
\ifdefined \LF
\usepackage[left=2.0cm, top=2.0cm, landscape]{geometry}  % for large format landscape
\else
\usepackage[left=2.0cm, top=2.0cm]{geometry}
\fi
\usepackage{fancyhdr}
\pagestyle{fancy}
\fancyhead{}
\lhead{CS 495/595}
\chead{Lab 1 Report}
\rhead{Alexander DuPree}
\begin{document}

\ifdefined \LF
{\Large     % large print start
\fi

  \section*{Introduction}
  \noindent

  This report presents the vulnerabilities and exploits used in the \textit{Broken Access
  Control} and \textit{SSRF, XXE, Sensitive Data Exposure} labs. Each section will describe 
  a vulnerability and explain how it was exploited by providing screenshots or accompnying
  scripts.
  
  \section*{Directory Traversal}
  Directory, or file path, traversal is a vulnerability that involves reading arbitrary files
  from a web server. This is generally accomplished by including standard linux file path
  sequences in the URL. \hfill \break
  
  \noindent\textbf{Lab 1:} The server does not filter the input, simply appending 
  '../../etc/passwd' to the URL is sufficient enough to access the protected file. 
  
  \begin{figure}[h!]
	\centering
	\includegraphics[width=1\linewidth]{file-traversal1.png}
	\caption[img]{File path traversal, simple case}
	\label{fig:P1compileP0-1}
  \end{figure}

  \noindent\textbf{Lab 2:} Solved by modifying the 'filename' parameter in the GET request
  that retrieves the images for the product images. 
  
  \begin{figure}[h!]
	\centering
	\includegraphics[width=1\linewidth]{file-traversal2.png}
	\caption[img]{File path traversal, traversal sequences blocked with absolute path bypass}
	\label{fig:P1compileP0-1}
  \end{figure}
  
  \pagebreak

  \noindent\textbf{Lab 3:} The server strips out file path sequences from the request, however
  it does not do so recursively. Appending a nested file path sequence ('....//....//....//etc/passwd')
  can solve the lab. 
  
  \begin{figure}[h!]
	\centering
	\includegraphics[width=1\linewidth]{file-traversal3.png}
	\caption[img]{File path traversal, traversal sequences stripped non-recursively}
	\label{fig:P1compileP0-1}
  \end{figure}

  \noindent\textbf{Lab 4:} Similar to lab 2, we change the 'filename' parameter to include 
  a file traversal sequence. This time, however, we must encode the '/' characters like this:
  '..\%252f..\%252f..\%252fetc/passwd'


  \begin{figure}[h!]
	\centering
	\includegraphics[width=1\linewidth]{file-traversal4.png}
	\caption[img]{File path traversal, traversal sequences stripped with superfluous URL-decode}
	\label{fig:P1compileP0-1}
  \end{figure}

  \noindent\textbf{Lab 5:} Now the server validates the start of the path. However we can 
  still leave the directory with file path sequences like this: '/var/www/images/../../../etc/passwd'.

  \begin{figure}[h!]
	\centering
	\includegraphics[width=1\linewidth]{file-traversal5.png}
	\caption[img]{File path traversal, validation of start of path}
	\label{fig:P1compileP0-1}
  \end{figure}

\pagebreak

  \noindent\textbf{Lab 6:} Now the server requires that the supplied filename must end in the
  '.png' extension. However this is easily bypassed by providing a null byte encoding. We 
  change the 'filename' parameter to include this: '../../../etc/passwd\%00.png'

  \begin{figure}[h!]
	\centering
	\includegraphics[width=1\linewidth]{file-traversal6.png}
	\caption[img]{File path traversal, validation of file extension with null byte bypass}
	\label{fig:P1compileP0-1}
  \end{figure}

  \section*{WFP1: File Upload}
  File uploads are a potent vector of attack for adversaries. By uploding malicious files and
  subsequently visiting them we can direct the server to execute malicious code for us. \hfill \break
  
  \noindent\textbf{Example 1:} From the URL we can determine that the web application executes
  php code. As such, by uploading a php with the contents \code{<?php system('ls -la'); ?>}
  and accessing it we were able to have the server execute the 'ls' command on their system. 
  
  \begin{figure}[h!]
	\centering
	\includegraphics[width=1\linewidth]{fileupload1.png}
	\caption[img]{Current Server Directory}
	\label{fig:P1compileP0-1}
  \end{figure}

  \noindent\textbf{Example 2:} Attempting to use the same file in the previous example does 
  not work for this lab because the site filters the .php extension. However after playing 
  around with the file extension we were able to upload a '.phtml' file to the site and 
  execute the 'pwd' command. 
  
  \begin{figure}[h!]
	\centering
	\includegraphics[width=1\linewidth]{fileupload2_fail.png}
	\caption[img]{Site filters .php files}
	\label{fig:P1compileP0-1}
  \end{figure}

  \begin{figure}[h!]
	\centering
	\includegraphics[width=1\linewidth]{fileupload2.png}
	\caption[img]{Successful upload of .phtml file}
	\label{fig:P1compileP0-1}
  \end{figure}

  \section*{WFP2: Authorization}
  By logging in and probing the URL of the site, we can access pages that we are not 
  authorized to view. \hfill \break
  
  \noindent\textbf{Example 1:} 
  
  \begin{figure}[h!]
	\centering
	\includegraphics[width=1\linewidth]{wfp2-auth1.png}
	\caption[img]{Attempting to access private info after logging out}
	\label{fig:P1compileP0-1}
  \end{figure}

  \noindent\textbf{Example 2:} Because the URL gave us an assigned number, we are able to
  just increment the number to find data that does not belong to us. 
  
  \begin{figure}[h!]
	\centering
	\includegraphics[width=1\linewidth]{wfp2-auth2.png}
	\caption[img]{user2 infos 12}
	\label{fig:P1compileP0-1}
  \end{figure}

  \pagebreak

  \noindent\textbf{Example 3:} Similar to example 2, we are able to probe the url to find
  a link to other user profiles. The figure below shows us editing the information of
  'Confidential user2'
  
  \begin{figure}[h!]
	\centering
	\includegraphics[width=1\linewidth]{wfp2-auth3.png}
	\caption[img]{Editing Confidential User 2}
	\label{fig:P1compileP0-1}
  \end{figure}

  \section*{Server Side Request Forgery}
  Server Side Request Forgery (SSRF) is a vulnerability that adversaries exploit to get 
  a web server to make arbitrary HTTP requests on their behalf. The result of which can 
  cause unauthorized access to sensitive data and even malicious command execution. \hfill \break
  
  \noindent\textbf{Lab 1:} After browsing the lab site, we notice that to get the stock of 
  a product, the client can send a POST request with the HTTP address of the 'stockApi'. With 
  a simple python script we can modify the stockApi request parameter and induce the 
  server to bypass security measures to let us access the `admin/' page. 
  
  \begin{figure}[h!]
	\centering
	\includegraphics[width=1\linewidth]{ssrf-py-1.png}
	\caption[img]{Python script to access admin interface}
	\label{fig:P1compileP0-1}
  \end{figure}

  \begin{figure}[h!]
	\centering
	\includegraphics[width=1\linewidth]{ssrf1.png}
	\caption[img]{Basic SSRF against the local server}
	\label{fig:P1compileP0-1}
  \end{figure}

  \pagebreak

  \noindent\textbf{Lab 2:} This lab was very interesting. We were required to access
  the back end admin interface to delete 'Carlos' profile. This required us to find 
  the internal server hosting the admin interface by probing potential IP addresses.
  
  \begin{figure}[h!]
	\centering
	\includegraphics[width=1\linewidth]{ssrf-py-2.png}
	\caption[img]{Script attempts to access admin interface by changing the ip address after each iteration}
	\label{fig:P1compileP0-1}
  \end{figure}

  \begin{figure}[h!]
	\centering
	\includegraphics[width=1\linewidth]{ssrf2.png}
	\caption[img]{Basic SSRF against another back-end system}
	\label{fig:P1compileP0-1}
  \end{figure}

  \pagebreak

  \noindent\textbf{Lab 3:} Now the server black lists certian inputs. However,
  after some encoding trickery we can still get the server to delete 'Carlos'. 
  
  \begin{figure}[h!]
	\centering
	\includegraphics[width=1\linewidth]{ssrf-py-3.png}
	\caption[img]{Simple script to access admin interface}
	\label{fig:P1compileP0-1}
  \end{figure}

  \begin{figure}[h!]
	\centering
	\includegraphics[width=1\linewidth]{ssrf3.png}
	\caption[img]{SSRF with blacklist-based input filter}
	\label{fig:P1compileP0-1}
  \end{figure}

  \section*{XML External Entity Injection}
  XML External Entity (XXE) injection is a vulnerability that attackers use to view
  files on the server, and even access backend of external systems. This is done through
  the hijacking of the applications proccessing of XML data. \hfill \break
  
  \noindent\textbf{Lab 1:} After browsing the lab site, we notice that the 'Check Stock' 
  button issues a POST requests with XML data to process the request. A simple python
  script and modify this request and access the /etc/passwd file.
  
  \begin{figure}[h!]
	\centering
	\includegraphics[width=1\linewidth]{xxe-py1.png}
	\caption[img]{Python script for XXE injection}
	\label{fig:P1compileP0-1}
  \end{figure}

  \begin{figure}[h!]
	\centering
	\includegraphics[width=1\linewidth]{xxe1.png}
	\caption[img]{Exploiting XXE using external entities to retrieve files}
	\label{fig:P1compileP0-1}
  \end{figure}

  \noindent\textbf{Lab 2:} XXE injection can also be used for SSRF. In this lab we 
  access the admin security credentials by using XXE to explore the API backend.
  
  \begin{figure}[h!]
	\centering
	\includegraphics[width=1\linewidth]{xxe-py2.png}
	\caption[img]{Python script for XXE injection and SSRF}
	\label{fig:P1compileP0-1}
  \end{figure}

  \begin{figure}[h!]
	\centering
	\includegraphics[width=1\linewidth]{xxe2.png}
	\caption[img]{Exploiting XXE to perform SSRF attacks}
	\label{fig:P1compileP0-1}
  \end{figure}

  \pagebreak

  \noindent\textbf{Lab 3:} In this lab we don't control the entire XML document, and can't
  carry out a traditional XXE attack. However, by using Xinclude we can create a proccessing
  error and have the file contents reported back to the client.
  
  \begin{figure}[h!]
	\centering
	\includegraphics[width=1\linewidth]{xxe-py3.png}
	\caption[img]{Python script for XXE and Xinclude exploit}
	\label{fig:P1compileP0-1}
  \end{figure}

  \begin{figure}[h!]
	\centering
	\includegraphics[width=1\linewidth]{xxe3.png}
	\caption[img]{Exploiting XXE to perform SSRF attacks}
	\label{fig:P1compileP0-1}
  \end{figure}

  \pagebreak

  \noindent\textbf{Lab 4:} XXE attacks can also be carried out with XML-based file
  uploads like 'svg'. In this lab we access the retrieve the server hostname by uploading
  a malicious svg file as an avatar for a blog post comment. 
  
  \begin{figure}[h!]
	\centering
	\includegraphics[width=1\linewidth]{xxe-svg4.png}
	\caption[img]{XXE attack with SVG file}
	\label{fig:P1compileP0-1}
  \end{figure}

  \begin{figure}[h!]
	\centering
	\includegraphics[width=1\linewidth]{xxe4.png}
	\caption[img]{Exploiting XXE via image file upload}
	\label{fig:P1compileP0-1}
  \end{figure}

\ifdefined \LF
} % large print end
\fi

\end{document}