import os
import json
import requests
import webbrowser
from datetime import datetime

TEMP_DIR     = os.getenv("CTF_TEMP")

def login():
    """ Logs into cs495 oregin ctf site and returns the session"""

    loginURL = "http://cs495.oregonctf.org/login"

    loginPayLoad = { "login" : os.getenv("CTF_LOGIN")
                   , "pwd"   : os.getenv("CTF_PWD") }

    session = requests.Session()

    session.post(loginURL, data=loginPayLoad)

    return session

def dump(response, file=''):
    if file == '':
        file = f'{TEMP_DIR}/resp.html'

    """ Dumps response headers and text into the specified file """
    with open(file, 'w') as f:
        # Write Headers
        f.write('<!--\n')
        for key, val in response.headers.items():
            f.write(f'{key} : {val}\n')
        f.write('-->\n')

        # Write HTML
        f.write(response.text)

def preview(response):
    """ Opens the dumped response in the browser """
    file = f'{TEMP_DIR}/resp-preview.html'
    dump(response, file)

    webbrowser.open(file)