import CTF
from bs4 import BeautifulSoup

def main():
    """ Lesson 3: Poor Data Validation"""

    # Ajax post endpoint for the leform on the page
    url = "http://cs495.oregonctf.org/lessons/4d8d50a458ca5f1f7e2506dd5557ae1f7da21282795d0ed86c55fefe41eb874f"

    session = CTF.login()

    # Modify userdata  parameter in form
    resp = session.post(url, data={"userdata" : "-42"})

    # Parse response for the result key
    soup = BeautifulSoup(resp.text, 'html.parser')

    print(soup.find_all(id="theKey"))

if __name__ == "__main__":
    main()
    