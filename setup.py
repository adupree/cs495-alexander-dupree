from setuptools import setup, find_packages
from os import path

here = path.abspath(path.dirname(__file__))

with open(path.join(here, 'README.md'), encoding='utf-8') as f:
    long_description = f.read()

setup(
        name = 'CTF',
        version = '1.0.0a',
        description='Script library for CS495.oregonctf.org',
        long_description=long_description,
        long_description_content_type='text/markdown',
        url='https://gitlab.com/adupree/cs495-alexander-dupree',
        author='Alexander Dupree',
        author_email='adupree@pdx.edu',
        classifiers=[
            'Development Status :: 3 - Alpha',
            'Environment :: Console',
            'Programming Language :: Python :: 3'
            ],
        keywords='CTF',
        py_modules= ['CTF'],
        python_requires='>=3.5',
        install_requires=['requests'],
        )