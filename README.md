# cs495-alexander-dupree

CS495 Cloud and Web Security at Portland State University

## What's in this Repo?

- hw/ <- Contains python scripts and files related to the cs495.oregonctf.org CTF site. 
- labs/ <- Contains scripts, documents, and images related to the labs portion of the class

The other folders are generally names of the homework assignment programs. 
